import java.util.Stack;
import java.lang.ArithmeticException;

public class MoteurRPN {
	
	public Stack<Integer> operande;
	public Stack<Operation> operateur;

	public MoteurRPN () {
		operande = new Stack<Integer>();
		operateur = new Stack<Operation>();
	}
	
	public int calcul() {
		int op1 = operande.pop();
		int op2 = operande.pop();
		Operation op = operateur.pop();
		return op.eval(op1,op2);
	}
	
	public int test() {
		
	}
	
	public void addOperande (int op) {
		operande.push(op);
	}
	
	public void addOperateur (Operation op) {
		operateur.push(op);
	}
	
	public void printOperandes () {
		int i;
		for (i=0; i<operande.size();i++) {
			System.out.println(operande.pop());
		}
	}
	
	public void printOperateurs () {
		int i;
		for (i=0; i<operateur.size();i++) {
			System.out.println(operateur.pop());
		}
	}

}
