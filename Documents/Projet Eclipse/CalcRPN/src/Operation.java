
public enum Operation {
	
	PLUS ("+") {
		public int eval(int op1, int op2) {
			return op1+op2;
		}
	}, MOINS ("-") {
		public int eval(int op1, int op2) {
			return op1-op2;
		}
	}, MULT ("*") {
		public int eval(int op1, int op2) {
			return op1*op2;
		}
	}, DIV ("/") {
		public int eval(int op1, int op2) {
			return op1/op2;
		}
	};
	
	String symbole;
	
	Operation (String symbole) {
		this.symbole = symbole;
	}
	
	public abstract int eval (int op1, int op2);
	
}
